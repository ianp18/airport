package Airport.src;

public class Destination {
    private String countryName;
    private String cityName;
    // private int depatureTime;
    // private int arrivalTime;
    private boolean hasPlaneArrived;

    public Destination(String countryName, String cityName) {
        this.countryName = countryName;
        this.cityName = cityName;
    }

    public String getCountryName() {
        return this.countryName;
    }

    public String getCityName() {
        return this.cityName;
    }

    /*
     * public Destination(int arrivalTime) {
     * this.arrivalTime = arrivalTime;
     * }
     * 
     * public int getDepartureTime() {
     * return this.depatureTime;
     * }
     * 
     * public int getArrivalTime() {
     * return this.arrivalTime;
     * }
     */

    public Boolean hasPlaneArrived() {
        hasPlaneArrived = true;
        return hasPlaneArrived;
    }

}
