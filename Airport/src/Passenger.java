package Airport.src;

public class Passenger {
    private String name;
    private int passportNumber;
    private BoardingPass boardingPass;
    private Bag bags;
    private Airport airportName;
    private String flightNo;

    public Passenger(String name, int passportNumber) {
        this.name = name;
        this.passportNumber = passportNumber;
    }

    public String getName() {
        return name;
    }

    public int getpassportNumber() {
        return passportNumber;
    }

    public void setBoardingPass(BoardingPass pass) {
        this.boardingPass = pass;
    }

    public BoardingPass getBoardingPass() {
        return boardingPass;
    }

    public void setBag(Bag bag) {
        this.bags = bag;
    }

    public Bag getBag() {
        Bag bag = this.bags;
        this.bags = null;
        return bag;
    }

    public void setAirport(String string) {
        this.airportName = airportName;
    }

    public Airport getAirport() {
        return this.airportName;
    }

    public void setAirTicket(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getFlightNo() {
        return this.flightNo;
    }

    public void setFlightNo(String string) {
    }
}