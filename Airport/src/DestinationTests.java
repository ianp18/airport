package Airport.src;

import static org.junit.Assert.*;

import org.junit.Test;

public class DestinationTests {
    @Test
    public void test_destination() {
        Destination destination = new Destination("USA", "New York");
        assertEquals(destination.getCountryName(), "USA");
        assertEquals(destination.getCityName(), "New York");
    }

    @Test
    public void hasPlaneArrived() {
        Destination destination = new Destination("USA", "New York");
        assertEquals(destination.hasPlaneArrived(), true);
    }
}
