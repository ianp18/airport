package Airport.src;

import static org.junit.Assert.*;

import org.junit.Test;

public class AirportTests {
    @Test
    public void airportHasAName() {
        Airport airport = new Airport("MAN");
        assertEquals(airport.getAirportName(), "MAN");
    }

    @Test
    public void parkPlanesAtAnAirport() {
        Plane plane = new Plane("LA993");
        Airport manchester = new Airport("MAN");
        manchester.landPlane(plane);
        assertEquals(manchester.planeAt("LA993"), plane);
    }

    @Test
    public void hasCheckInDesks() {
        Airport liverpool = new Airport("LIV");
        assertTrue(message, condition);
    }
    
}
