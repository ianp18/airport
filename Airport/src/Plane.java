package Airport.src;

public class Plane {

    Passenger[][] seats = {
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
            { null, null, null },
    };

    private String planeName;

    public Plane(String planeName) {
        this.planeName = planeName;
    }

    public Plane() {
    }

    public String getPlane() {
        return planeName;
    }

    public Boolean isSeatEmpty(BoardingPass bPass) {
        int row = bPass.getSeat()[0];
        int seat = bPass.getSeat()[1];
        return this.seats[row][seat] == null;

    }

    public void boardPassenger(Passenger passenger) {
        int row = passenger.getBoardingPass().getSeat()[0];
        int seat = passenger.getBoardingPass().getSeat()[1];
        this.seats[row][seat] = passenger;

    }
}
