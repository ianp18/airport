package Airport.src;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlaneTests {
    @Test
    public void aPassengerCanBoardAPlane() {
        Passenger passenger = new Passenger("Ian", 18028028);
        BoardingPass BP = new BoardingPass(1, 1);
        passenger.setBoardingPass(BP);
        Plane plane = new Plane();
        assertEquals(plane.isSeatEmpty(BP), true);
        plane.boardPassenger(passenger);
        assertEquals(plane.isSeatEmpty(BP), false);


    }

    
}
