package Airport.src;

import static org.junit.Assert.*;

import org.junit.Test;

public class CheckInDeskTests {
    @Test
    public void checkInAPassengerTheyGetABoardingPass() {
        Passenger passenger = new Passenger("Ian", 18028028);
        CheckInDesk desk1 = new CheckInDesk(new Airport("LHR"));
        assertNull(passenger.getBoardingPass());
        desk1.checkIn(passenger);
        assertNotNull(passenger.getBoardingPass());
    }

    @Test
    public void checkInAPassengerTheirBagsAreTaken() throws Exception {
        Bag bag = new Bag(15);
        Passenger passenger = new Passenger("Ian", 18028028);
        passenger.setBag(bag);
        CheckInDesk desk1 = new CheckInDesk(new Airport("LHR"));
        desk1.checkIn(passenger);
        assertNull(passenger.getBag());
    }

    @Test
    public void shouldHaveAccessToPlanes() {
        Airport airport = new Airport("LHR");
        Plane plane = new Plane("TE55no");
        airport.landPlane(plane);
        Passenger passenger = new Passenger("Emily", 1204824);
        passenger.setFlightNo("TE55no");
        airport.checkInDesks[0].checkIn(passenger);
        assertFalse(plane.isSeatEmpty(passenger.getBoardingPass()));
    }

}
