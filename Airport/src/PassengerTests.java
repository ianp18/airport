package Airport.src;

import static org.junit.Assert.*;

import org.junit.Test;

public class PassengerTests {
    @Test
    public void test_name() {
        Passenger passenger = new Passenger("Ian", 18028028);
        assertEquals(passenger.getName(), "Ian");
        assertEquals(passenger.getpassportNumber(), 18028028);
    }

    @Test
    public void aPassengerCanHaveaBoardingPass() {
        Passenger passenger = new Passenger("Ian", 18028028);
        BoardingPass pass = new BoardingPass(1, 1);
        passenger.setBoardingPass(pass);
        assertSame(passenger.getBoardingPass(), pass);
    }

    @Test
    public void aPassengerGoesToAnAirport() {
        Airport Manchester = new Airport("MAN");
        Passenger passenger = new Passenger("Ian", 18028028);
        assertNull(passenger.getAirport());
        passenger.setAirport("MAN");
        assertSame(passenger.getAirport(), "MAN");

    }

    @Test
    public void aPassengerCanHaveABag() throws Exception {
        Passenger passenger = new Passenger("Ian", 18028028);
        Bag bag = new Bag(13);
        passenger.setBag(bag);
        assertSame(passenger.getBag(), bag);
    }

    @Test
    public void aPassengerChecksInBag() throws Exception {
        Passenger passenger = new Passenger("Ian", 18028028);
        Bag bag = new Bag(13);
        passenger.setBag(bag);
        passenger.getBag();
        assertSame(passenger.getBag(), null);
    }

}
